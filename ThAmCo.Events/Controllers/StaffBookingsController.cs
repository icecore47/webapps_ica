﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ThAmCo.Events.Data;

namespace ThAmCo.Events.Views
{
    public class StaffBookingsController : Controller
    {
        private readonly EventsDbContext _context;

        public StaffBookingsController(EventsDbContext context)
        {
            _context = context;
        }

        // GET: StaffBookings
        public async Task<IActionResult> Index(int id)
        {
            var eventsDbContext = _context.Staffed
                .Include(g => g.Staff)
                .Include(g => g.Event)
                .Where(g => g.EventId == id);
            return View(await eventsDbContext.ToListAsync());
        }

        // GET: StaffBookings/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var StaffBooking = await _context.Staffed
                .Include(g => g.Staff)
                .Include(g => g.Event)
                .FirstOrDefaultAsync(m => m.StaffId == id);
            if (StaffBooking == null)
            {
                return NotFound();
            }
        
            return View(StaffBooking);
        }

        // GET: StaffBookings/Create
        public IActionResult Create([FromQuery]int? eventId)
        {
            if (eventId == null) {
                return BadRequest();
            }


            var allStaff = _context.Staffs.ToList();
            var CurStaff = _context.Staffed
                .Where(g => g.EventId == eventId)
                .ToList();
            allStaff.RemoveAll(ac => CurStaff.Any(cg => cg.StaffId == ac.Id));



            ViewData["StaffId"] = new SelectList(allStaff, "Id", "Email");

            ViewData["EventId"] = new SelectList(_context.Events, "Id", "Title", eventId);
            var eventname = _context.Events.Find(eventId);
            if (eventname == null) {
                return BadRequest();
            }
            ViewData["EventN"] = eventname.Title;
            return View();
        }

        // POST: StaffBookings/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("StaffId,EventId,Attended")] StaffBookings StaffBooking)
        {
            if (ModelState.IsValid)
                if (_context.Staffed.Any(g => g.EventId == StaffBooking.EventId && g.StaffId == StaffBooking.StaffId))
                {
                    ModelState.AddModelError(String.Empty, "Staff Already Registered For Event");
                }
                else 
                    {
                _context.Add(StaffBooking);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index","Events");
            }
            ViewData["StaffId"] = new SelectList(_context.Staffs, "Id", "Email", StaffBooking.StaffId);
            ViewData["EventId"] = new SelectList(_context.Events, "Id", "Title", StaffBooking.EventId);
            var eventname = await _context.Events.FindAsync(StaffBooking.EventId);
            if (eventname == null)
            {
                return BadRequest();
            }
            ViewData["EventN"] = eventname.Title;

            return View(StaffBooking);
        }












        // GET: StaffBookings/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var StaffBooking = await _context.Staffed.FindAsync(id);
            if (StaffBooking == null)
            {
                return NotFound();
            }
            ViewData["StaffId"] = new SelectList(_context.Staffs, "Id", "Email", StaffBooking.StaffId);
            ViewData["EventId"] = new SelectList(_context.Events, "Id", "Title", StaffBooking.EventId);
            return View(StaffBooking);
        }

        // POST: StaffBookings/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("StaffId,EventId,Attended")] StaffBookings StaffBooking)
        {
            if (id != StaffBooking.StaffId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(StaffBooking);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StaffBookingExists(StaffBooking.StaffId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["StaffId"] = new SelectList(_context.Staffs, "Id", "Email", StaffBooking.StaffId);
            ViewData["EventId"] = new SelectList(_context.Events, "Id", "Title", StaffBooking.EventId);
            return View(StaffBooking);
        }

        // GET: StaffBookings/Delete/5
        public async Task<IActionResult> Delete(int? Eventid, int? Staffid)
        {
            if (Eventid == null ||Staffid == null)
            {
                return NotFound();
            }

            var StaffBooking = await _context.Staffed
                .Include(g => g.Staff)
                .Include(g => g.Event)
                .FirstOrDefaultAsync(m => m.StaffId == Staffid && m.EventId == Eventid);
            if (StaffBooking == null)
            {
                return NotFound();
            }

            return View(StaffBooking);
        }

        // POST: StaffBookings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int? Eventid, int? Staffid)
        {

            var StaffBooking = await _context.Staffed
                .Include(g => g.Staff)
                .Include(g => g.Event)
                .FirstOrDefaultAsync(m => m.StaffId == Staffid && m.EventId == Eventid);

         
            _context.Staffed.Remove(StaffBooking);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool StaffBookingExists(int id)
        {
            return _context.Staffed.Any(e => e.StaffId == id);
        }
    }
}
