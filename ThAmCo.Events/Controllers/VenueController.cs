﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ThAmCo.Events.Services;

namespace ThAmCo.Events.Controllers
{
    public class VenueController : Controller
    {
        private readonly ILogger<VenueController> _Log;
        private readonly IConfiguration _Con;
        public VenueController(ILogger<VenueController> Log, IConfiguration Config)
        {
            _Log = Log;
            _Con = Config;
        }

        // GET: Venues
        public async Task<ActionResult> Index(IFormCollection collection)
        {

            var Code = collection["Code"];
            var Start = collection["Start"];
            var End  = collection["End"];

            var client = new HttpClient();
            client.BaseAddress = new Uri(_Con["VenuesBAseURI"]);
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");


            IEnumerable<VenueGetDto> venues = null;
            try
            {
                var response = await client.GetAsync("/api/Availability?eventType="+Code+ "&beginDate=" + Start + "&endDate=" + End + "");
                response.EnsureSuccessStatusCode();
                venues = await response.Content.ReadAsAsync<IEnumerable<VenueGetDto>>();
                venues.ToList();
                
            
            }
            catch (HttpRequestException e)
            {
                _Log.LogError(e.Message);
                venues = Array.Empty<VenueGetDto>();

            }
            ViewBag.Type = Code;
            return View(venues);
        }

        // GET: Venues/Details/5
        public async Task<ActionResult> Details(String id)
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri(_Con["VenuesBAseURI"]);
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");

            IEnumerable<VenueGetDto> venues = null;
            VenueGetDto ven = null;
            try
            {
                var response = await client.GetAsync("/api/Availability?eventType=WED&beginDate=2010/11/15&endDate=2018/11/15");
                response.EnsureSuccessStatusCode();
                venues = await response.Content.ReadAsAsync<IEnumerable<VenueGetDto>>();
                ven = venues.Select(v => new VenueGetDto
                {
                    Capacity = v.Capacity,
                    Code = v.Code,
                    Description = v.Description,
                    Name = v.Name,
                    Cost = v.Cost,
                    Date = v.Date
                }).Where(v => v.Code == id)
                 .FirstOrDefault();
            }
            catch (HttpRequestException e)
            {
                _Log.LogError(e.Message);
                venues = null;

            }
            return View(ven);




        }
        public ActionResult Create()
        {
            return View();
        }

        // POST: Search/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {

            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }


    }
}
