﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ThAmCo.Events.Data;

namespace ThAmCo.Events.Views
{
    public class GuestBookingsController : Controller
    {
        private readonly EventsDbContext _context;

        public GuestBookingsController(EventsDbContext context)
        {
            _context = context;
        }

        // GET: GuestBookings
        public async Task<IActionResult> Index(int id)
        {


            var eventsDbContext = _context.Guests
                .Include(g => g.Customer)
                .Include(g => g.Event)
               .Where(g => g.EventId == id);

            return View(await eventsDbContext.ToListAsync());
        }

        // GET: GuestBookings/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var guestBooking = await _context.Guests
                .Include(g => g.Customer)
                .Include(g => g.Event)
                .FirstOrDefaultAsync(m => m.CustomerId == id);
            if (guestBooking == null)
            {
                return NotFound();
            }
        
            return View(guestBooking);
        }

        // GET: GuestBookings/Create
        public IActionResult Create([FromQuery]int? eventId)
        {
            if (eventId == null) {
                return BadRequest();
            }


            var allCust = _context.Customers.ToList();
            var CurGuest = _context.Guests
                .Where(g => g.EventId == eventId)
                .ToList();
            allCust.RemoveAll(ac => CurGuest.Any(cg => cg.CustomerId == ac.Id));



            ViewData["CustomerId"] = new SelectList(allCust, "Id", "Email");

            ViewData["EventId"] = new SelectList(_context.Events, "Id", "Title", eventId);
            var eventname = _context.Events.Find(eventId);
            if (eventname == null) {
                return BadRequest();
            }
            ViewData["EventN"] = eventname.Title;
            return View();
        }

        // POST: GuestBookings/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CustomerId,EventId,Attended")] GuestBooking guestBooking)
        {
            if (ModelState.IsValid)
                if (_context.Guests.Any(g => g.EventId == guestBooking.EventId && g.CustomerId == guestBooking.CustomerId))
                {
                    ModelState.AddModelError(String.Empty, "Customer Already Registered For Event");
                }
                else 
                    {
                _context.Add(guestBooking);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index","Events");
            }
            ViewData["CustomerId"] = new SelectList(_context.Customers, "Id", "Email", guestBooking.CustomerId);
            ViewData["EventId"] = new SelectList(_context.Events, "Id", "Title", guestBooking.EventId);
            var eventname = await _context.Events.FindAsync(guestBooking.EventId);
            if (eventname == null)
            {
                return BadRequest();
            }
            ViewData["EventN"] = eventname.Title;

            return View(guestBooking);
        }

       

        // GET: GuestBookings/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var guestBooking = await _context.Guests.FindAsync(id);
            if (guestBooking == null)
            {
                return NotFound();
            }
            ViewData["CustomerId"] = new SelectList(_context.Customers, "Id", "Email", guestBooking.CustomerId);
            ViewData["EventId"] = new SelectList(_context.Events, "Id", "Title", guestBooking.EventId);
            return View(guestBooking);
        }

        // POST: GuestBookings/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CustomerId,EventId,Attended")] GuestBooking guestBooking)
        {
            if (id != guestBooking.CustomerId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(guestBooking);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!GuestBookingExists(guestBooking.CustomerId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CustomerId"] = new SelectList(_context.Customers, "Id", "Email", guestBooking.CustomerId);
            ViewData["EventId"] = new SelectList(_context.Events, "Id", "Title", guestBooking.EventId);
            return View(guestBooking);
        }

        // GET: GuestBookings/Delete/5
        public async Task<IActionResult> Delete(int? Eventid, int? Customerid)
        {
            if (Eventid == null || Customerid == null)
            {
                return NotFound();
            }

            var guestBooking = await _context.Guests
                .Include(g => g.Customer)
                .Include(g => g.Event)
                .FirstOrDefaultAsync(m => m.CustomerId == Customerid && m.EventId == Eventid);
            if (guestBooking == null)
            {
                return NotFound();
            }

            return View(guestBooking);
        }

        // POST: GuestBookings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int? Eventid, int? Customerid)
        {
            var guestBooking = await _context.Guests
         .Include(g => g.Customer)
         .Include(g => g.Event)
         .FirstOrDefaultAsync(m => m.CustomerId == Customerid && m.EventId == Eventid);


            _context.Guests.Remove(guestBooking);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool GuestBookingExists(int id)
        {
            return _context.Guests.Any(e => e.CustomerId == id);
        }
    }
}
