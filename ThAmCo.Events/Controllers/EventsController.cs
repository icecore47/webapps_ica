﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ThAmCo.Events.Models;
using ThAmCo.Venues.Models;

namespace ThAmCo.Events.Data
{
    public class EventsController : Controller
    {
        private readonly EventsDbContext _context;
        private readonly ILogger<EventsController> _Log;
        private readonly IConfiguration _Con;




        public EventsController(EventsDbContext context, ILogger<EventsController> Log, IConfiguration Config)
        {
            _context = context;
            _Log = Log;
            _Con = Config;
        }

        // GET: Events
        public async Task<IActionResult> Index()
        {
            return View(await _context.Events.ToListAsync());
        }

        // GET: Events/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var e = await _context.Events
                .Include(p => p.Bookings)
                .Select(p=> new EventDetailsViewModel
                {
                              Id =p.Id,
                              Date=p.Date,
                              Duration=p.Duration, 
                              TypeId=p.TypeId,
                              Guests =_context.Guests    .Where(g => g.EventId == p.Id)
                                                         .Select(g => new CustomerEventViewModel 
                                                     {
                                                          CustomerId = g.CustomerId,
                                                          EventId =g.EventId,
                                                          Email = g.Customer.Email,
                                                          FirstName =g.Customer.FirstName,
                                                          Surname = g.Customer.Surname
                                                     }),
                            Staff = _context.Staffed     .Where(s => s.EventId == p.Id)
                                                         .Select(s => new StaffEventViewModel
                                                         {
                                                             StaffId = s.StaffId,
                                                             EventId = s.EventId,
                                                             Email = s.Staff.Email,
                                                             FirstName = s.Staff.FirstName,
                                                             Surname = s.Staff.Surname

                                                         }),




                })
                .FirstOrDefaultAsync(m => m.Id == id);



            if (e == null)
            {
                return NotFound();
            }

            return View(e);
        }

        // GET: Events/Create
        public IActionResult Create()
        {
            return View();
        }
        public IActionResult CreateVenue([Bind("Code,Date,EventType")] EventVenueVM EV)
        {
            Event e = new Event();
            e.Date = EV.Date;
            e.VenueCode = EV.Code;
            e.Duration = null;
            e.Title = "Insert Title Here";
            e.TypeId = EV.EventType;

            return View(e);
        }

        // POST: Events/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Title,Date,Duration,TypeId")] Event @event)
        {
            if (ModelState.IsValid)
            {
                _context.Add(@event);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(@event);
        }

        public async Task VenueRes(IFormCollection collection)
        {

            var Date = collection["Date"];
            var client = new HttpClient();
            client.BaseAddress = new Uri(_Con["VenuesBAseURI"]);


            var reservation = new ReservationDto
            {
                Reference = collection["Title"],
                EventDate = Convert.ToDateTime(Date),
                WhenMade = DateTime.Today,
                VenueCode = collection["VenueCode"],
                StaffId = "Thomas Black"
            };

            HttpResponseMessage response1 = await client.PostAsJsonAsync("api/reservations/", reservation);



        }

        // GET: Events/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @event = await _context.Events.FindAsync(id);
            if (@event == null)
            {
                return NotFound();
            }
            return View(@event);
        }

        // POST: Events/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,Duration")] EventEditViewModel @EventEditViewModel)
        {
            if (id != @EventEditViewModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var model = await _context.Events.FindAsync(id);
                    model.Title = EventEditViewModel.Title;
                    model.Duration = EventEditViewModel.Duration;

                    _context.Update(model);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EventExists(@EventEditViewModel.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(@EventEditViewModel);
        }

        // GET: Events/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @event = await _context.Events
                .FirstOrDefaultAsync(m => m.Id == id);
            if (@event == null)
            {
                return NotFound();
            }

            return View(@event);
        }

        // POST: Events/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var @event = await _context.Events.FindAsync(id);
            _context.Events.Remove(@event);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EventExists(int id)
        {
            return _context.Events.Any(e => e.Id == id);
        }
    }
}
