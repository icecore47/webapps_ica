﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ThAmCo.Events.Data.Migrations
{
    public partial class Staff : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Guests_Customers_CustomerId",
                schema: "thamco.events",
                table: "Guests");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Customers",
                schema: "thamco.events",
                table: "Customers");

            migrationBuilder.RenameTable(
                name: "Customers",
                schema: "thamco.events",
                newName: "Customer",
                newSchema: "thamco.events");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Customer",
                schema: "thamco.events",
                table: "Customer",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "Staff",
                schema: "thamco.events",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Surname = table.Column<string>(nullable: false),
                    FirstName = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Staff", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Staffed",
                schema: "thamco.events",
                columns: table => new
                {
                    StaffId = table.Column<int>(nullable: false),
                    EventId = table.Column<int>(nullable: false),
                    Attended = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Staffed", x => new { x.StaffId, x.EventId });
                    table.ForeignKey(
                        name: "FK_Staffed_Events_EventId",
                        column: x => x.EventId,
                        principalSchema: "thamco.events",
                        principalTable: "Events",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Staffed_Staff_StaffId",
                        column: x => x.StaffId,
                        principalSchema: "thamco.events",
                        principalTable: "Staff",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                schema: "thamco.events",
                table: "Staff",
                columns: new[] { "Id", "Email", "FirstName", "Surname" },
                values: new object[] { 1, "Thomas@example.com", "Thomas", "Black" });

            migrationBuilder.InsertData(
                schema: "thamco.events",
                table: "Staff",
                columns: new[] { "Id", "Email", "FirstName", "Surname" },
                values: new object[] { 2, "Jim@example.com", "Jim", "Black" });

            migrationBuilder.InsertData(
                schema: "thamco.events",
                table: "Staff",
                columns: new[] { "Id", "Email", "FirstName", "Surname" },
                values: new object[] { 3, "Harry@example.com", "Harry", "Black" });

            migrationBuilder.InsertData(
                schema: "thamco.events",
                table: "Staffed",
                columns: new[] { "StaffId", "EventId", "Attended" },
                values: new object[,]
                {
                    { 1, 1, true },
                    { 1, 2, false },
                    { 2, 1, false },
                    { 3, 2, false }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Staffed_EventId",
                schema: "thamco.events",
                table: "Staffed",
                column: "EventId");

            migrationBuilder.AddForeignKey(
                name: "FK_Guests_Customer_CustomerId",
                schema: "thamco.events",
                table: "Guests",
                column: "CustomerId",
                principalSchema: "thamco.events",
                principalTable: "Customer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Guests_Customer_CustomerId",
                schema: "thamco.events",
                table: "Guests");

            migrationBuilder.DropTable(
                name: "Staffed",
                schema: "thamco.events");

            migrationBuilder.DropTable(
                name: "Staff",
                schema: "thamco.events");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Customer",
                schema: "thamco.events",
                table: "Customer");

            migrationBuilder.RenameTable(
                name: "Customer",
                schema: "thamco.events",
                newName: "Customers",
                newSchema: "thamco.events");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Customers",
                schema: "thamco.events",
                table: "Customers",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Guests_Customers_CustomerId",
                schema: "thamco.events",
                table: "Guests",
                column: "CustomerId",
                principalSchema: "thamco.events",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
