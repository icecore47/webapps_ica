﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ThAmCo.Events.Data.Migrations
{
    public partial class Staff2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Guests_Customer_CustomerId",
                schema: "thamco.events",
                table: "Guests");

            migrationBuilder.DropForeignKey(
                name: "FK_Staffed_Staff_StaffId",
                schema: "thamco.events",
                table: "Staffed");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Staff",
                schema: "thamco.events",
                table: "Staff");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Customer",
                schema: "thamco.events",
                table: "Customer");

            migrationBuilder.RenameTable(
                name: "Staff",
                schema: "thamco.events",
                newName: "Staffs",
                newSchema: "thamco.events");

            migrationBuilder.RenameTable(
                name: "Customer",
                schema: "thamco.events",
                newName: "Customers",
                newSchema: "thamco.events");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Staffs",
                schema: "thamco.events",
                table: "Staffs",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Customers",
                schema: "thamco.events",
                table: "Customers",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Guests_Customers_CustomerId",
                schema: "thamco.events",
                table: "Guests",
                column: "CustomerId",
                principalSchema: "thamco.events",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Staffed_Staffs_StaffId",
                schema: "thamco.events",
                table: "Staffed",
                column: "StaffId",
                principalSchema: "thamco.events",
                principalTable: "Staffs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Guests_Customers_CustomerId",
                schema: "thamco.events",
                table: "Guests");

            migrationBuilder.DropForeignKey(
                name: "FK_Staffed_Staffs_StaffId",
                schema: "thamco.events",
                table: "Staffed");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Staffs",
                schema: "thamco.events",
                table: "Staffs");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Customers",
                schema: "thamco.events",
                table: "Customers");

            migrationBuilder.RenameTable(
                name: "Staffs",
                schema: "thamco.events",
                newName: "Staff",
                newSchema: "thamco.events");

            migrationBuilder.RenameTable(
                name: "Customers",
                schema: "thamco.events",
                newName: "Customer",
                newSchema: "thamco.events");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Staff",
                schema: "thamco.events",
                table: "Staff",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Customer",
                schema: "thamco.events",
                table: "Customer",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Guests_Customer_CustomerId",
                schema: "thamco.events",
                table: "Guests",
                column: "CustomerId",
                principalSchema: "thamco.events",
                principalTable: "Customer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Staffed_Staff_StaffId",
                schema: "thamco.events",
                table: "Staffed",
                column: "StaffId",
                principalSchema: "thamco.events",
                principalTable: "Staff",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
