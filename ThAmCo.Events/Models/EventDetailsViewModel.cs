﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ThAmCo.Events.Data
{
    public class EventDetailsViewModel
    {

            public int Id { get; set; }
            public string Title { get; set; }
            public DateTime Date { get; set; }
            public TimeSpan? Duration { get; set; }
            public string TypeId { get; set; }
            public IEnumerable<CustomerEventViewModel> Guests { get; set; }
            public IEnumerable<StaffEventViewModel> Staff { get; set; }
    }
    }




