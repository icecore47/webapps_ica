﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ThAmCo.Events.Data
{
 
    public class CustomerEventViewModel
    {
        public int CustomerId { get; set; }

        public string Surname { get; set; }
        
        public string FirstName { get; set; }

        public string Email { get; set; }

        public int EventId { get; set; }


    }
}


